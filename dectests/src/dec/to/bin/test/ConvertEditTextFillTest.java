package dec.to.bin.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;
import android.widget.EditText;
import data.TestData;
import dec.to.bin.DecToBinToHexFragment;
import dec.to.bin.DecimalToBinToHex;
import dec.to.bin.MainActivity;

public class ConvertEditTextFillTest extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mainActivity;
	private DecimalToBinToHex convert;

	DecToBinToHexFragment fragment;

	TestData testData = new TestData();

	public ConvertEditTextFillTest() {
		super(MainActivity.class);
	}

	public ConvertEditTextFillTest(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();
		fragment = (DecToBinToHexFragment) getActivity()
				.getSupportFragmentManager().findFragmentByTag(
						"dectobintohexTag");
		convert = fragment.convert;
	}

	public void testActivityFragmentViewObjectsAreNotNull() {
		assertNotNull(mainActivity);
		assertNotNull(convert);
		assertNotNull(convert.hexXEditText);
		assertNotNull(convert.decimalXEditText);
		assertNotNull(convert.convertButton);
	}

	@MediumTest
	public void testConvertDecimal() {
		for (int i = 0; i < testData.decString.length; i++) {
			final EditText decX = (EditText) convert.decimalXEditText;
			final Button convertX = (Button) convert.convertButton;
			final Button clear = (Button) convert.clearButton;

			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					clear.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, clear);
			getInstrumentation().waitForIdleSync();

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					decX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.decString[i]);
			getInstrumentation().waitForIdleSync();

			String s = convert.decimalXEditText.getText().toString();
			assertTrue(s.equals(testData.decString[i]));

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					convertX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, convertX);
			getInstrumentation().waitForIdleSync();

			String binActual = convert.binaryXEditText.getText().toString();
			String binExpected = testData.binString[i];
			assertTrue(binActual.equals(binExpected));

			String hexActual = convert.hexXEditText.getText().toString();
			String hexExpected = testData.hexString[i];
			assertTrue(hexActual.equals(hexExpected));
		}
	}

	@MediumTest
	public void testConvertHex() {
		for (int i = 0; i < testData.hexString.length; i++) {
			final EditText hexX = (EditText) convert.hexXEditText;
			final Button convertX = (Button) convert.convertButton;
			final Button clear = (Button) convert.clearButton;

			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					clear.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, clear);
			getInstrumentation().waitForIdleSync();

			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					hexX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.hexString[i]);
			getInstrumentation().waitForIdleSync();

			String s = convert.hexXEditText.getText().toString();
			assertTrue(s.equals(testData.hexString[i]));

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					convertX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, convertX);
			getInstrumentation().waitForIdleSync();

			String decActual = convert.decimalXEditText.getText().toString();
			String decExpected = testData.decString[i];
			assertTrue(decActual.equals(decExpected));

			String binActual = convert.binaryXEditText.getText().toString();
			String binExpected = testData.binString[i];
			assertTrue(binActual.equals(binExpected));
		}
	}

	@MediumTest
	public void testConvertBinary() {
		for (int i = 0; i < testData.decString.length; i++) {
			final EditText binX = (EditText) convert.binaryXEditText;
			final Button convertX = (Button) convert.convertButton;
			final Button clear = (Button) convert.clearButton;

			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					clear.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, clear);
			getInstrumentation().waitForIdleSync();

			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					binX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.binString[i]);
			getInstrumentation().waitForIdleSync();

			String s = convert.binaryXEditText.getText().toString();
			assertTrue(s.equals(testData.binString[i]));

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					convertX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			TouchUtils.clickView(this, convertX);
			getInstrumentation().waitForIdleSync();

			String decActual = convert.decimalXEditText.getText().toString();
			String decExpected = testData.decString[i];
			assertTrue(decActual.equals(decExpected));

			String hexActual = convert.hexXEditText.getText().toString();
			String hexExpected = testData.hexString[i];
			assertTrue(hexActual.equals(hexExpected));
		}
	}
}