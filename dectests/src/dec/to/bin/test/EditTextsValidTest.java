package dec.to.bin.test;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import data.TestData;
import dec.to.bin.DecToBinToHexFragment;
import dec.to.bin.DecimalToBinToHex;
import dec.to.bin.MainActivity;

public class EditTextsValidTest extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mainActivity;
	private DecimalToBinToHex convert;
	TestData testData = new TestData();

	DecToBinToHexFragment fragment;

	public EditTextsValidTest() {
		super(MainActivity.class);
	}

	public EditTextsValidTest(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();
		fragment = (DecToBinToHexFragment) getActivity()
				.getSupportFragmentManager().findFragmentByTag(
						"dectobintohexTag");
		convert = fragment.convert;
	}

	public void testActivityFragmentViewObjectsAreNotNull() {
		assertNotNull(mainActivity);
		assertNotNull(convert);
		assertNotNull(convert.hexXEditText);
		assertNotNull(convert.decimalXEditText);
		assertNotNull(convert.convertButton);
	}

	public void testDecXEditTextWriteAndClear() {
		final EditText pw = (EditText) convert.decimalXEditText;
		final Button clearButton = (Button) convert.clearButton;

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				pw.requestFocus();
			}
		});

		getInstrumentation().waitForIdleSync();
		getInstrumentation().sendStringSync("10");
		getInstrumentation().waitForIdleSync();

		String s = convert.decimalXEditText.getText().toString();
		assertTrue(s.equals("10"));

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				clearButton.performClick();
			}
		});

		getInstrumentation().waitForIdleSync();

		String s1 = convert.decimalXEditText.getText().toString();
		assertTrue(s1.equals(""));
	}

	public void testBinXEditTextWriteAndClear() {
		final EditText pw = (EditText) convert.binaryXEditText;
		final Button clearButton = (Button) convert.clearButton;
		
		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				clearButton.performClick();
			}
		});

		getInstrumentation().waitForIdleSync();

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				pw.requestFocus();
			}
		});

		getInstrumentation().waitForIdleSync();
		getInstrumentation().sendStringSync("10");
		getInstrumentation().waitForIdleSync();

		String s = convert.binaryXEditText.getText().toString();
		assertTrue(s.equals("10"));

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				clearButton.performClick();
			}
		});

		getInstrumentation().waitForIdleSync();

		String s1 = convert.binaryXEditText.getText().toString();
		assertTrue(s1.equals(""));
	}

	public void testHexXEditTextWriteAndClear() {
		final EditText pw = (EditText) convert.hexXEditText;
		final Button clearButton = (Button) convert.clearButton;

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				pw.requestFocus();
			}
		});

		getInstrumentation().waitForIdleSync();
		getInstrumentation().sendStringSync("10");
		getInstrumentation().waitForIdleSync();

		String s = convert.hexXEditText.getText().toString();
		assertTrue(s.equals("10"));

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				clearButton.performClick();
			}
		});

		getInstrumentation().waitForIdleSync();

		String s1 = convert.hexXEditText.getText().toString();
		assertTrue(s1.equals(""));
	}

	public void testOneValueOnConvertPress() {
		boolean row2[] = { true, false, false, true };
		boolean expected = row2[3];
		boolean actual = assertConvertsReturn(row2);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress2() {
		boolean row3[] = { true, false, true, false };
		boolean expected = row3[3];
		boolean actual = assertConvertsReturn(row3);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress3() {
		boolean row4[] = { true, true, false, false };
		boolean expected = row4[3];
		boolean actual = assertConvertsReturn(row4);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress4() {
		boolean row5[] = { true, true, true, false };
		boolean expected = row5[3];
		boolean actual = assertConvertsReturn(row5);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress5() {
		boolean row6[] = { false, false, false, false };
		boolean expected = row6[3];
		boolean actual = assertConvertsReturn(row6);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress6() {
		boolean row7[] = { false, false, true, true };
		boolean expected = row7[3];
		boolean actual = assertConvertsReturn(row7);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress7() {
		boolean row8[] = { false, true, true, false };
		boolean expected = row8[3];
		boolean actual = assertConvertsReturn(row8);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress8() {
		boolean row9[] = { false, true, false, true };
		boolean expected = row9[3];
		boolean actual = assertConvertsReturn(row9);
		assertTrue(actual == expected);
	}

	public void testOneValueOnConvertPress9() {
		boolean row10[] = { true, true, true, true };
		boolean result = assertConvertsReturn(row10);
		assertTrue(result != true);
	}

	private boolean assertConvertsReturn(boolean[] row) {
		// clear fields
		final Button clearButton = (Button) convert.clearButton;

		// Send string input value
		getInstrumentation().runOnMainSync(new Runnable() {
			@Override
			public void run() {
				clearButton.performClick();
			}
		});

		getInstrumentation().waitForIdleSync();

		// set each edit text with a value?
		if (row[0]) {
			final EditText decX = (EditText) convert.decimalXEditText;

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					decX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.decString[0]);
			getInstrumentation().waitForIdleSync();
		}

		if (row[1]) {
			final EditText binX = (EditText) convert.binaryXEditText;

			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					binX.requestFocus();
				}
			});

			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.binString[0]);
			getInstrumentation().waitForIdleSync();
		}

		if (row[2]) {
			final EditText hexX = (EditText) convert.hexXEditText;
			// Send string input value
			getInstrumentation().runOnMainSync(new Runnable() {
				@Override
				public void run() {
					hexX.requestFocus();
				}
			});
			getInstrumentation().waitForIdleSync();
			getInstrumentation().sendStringSync(testData.hexString[0]);
			getInstrumentation().waitForIdleSync();
		}

		boolean actual = convert.checkOneValue(
				convert.decimalXEditText.getText(),
				convert.binaryXEditText.getText(),
				convert.hexXEditText.getText());

		return actual;
	}
}