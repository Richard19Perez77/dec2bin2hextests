package dec.to.bin.test;

import java.math.BigInteger;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import data.TestData;
import dec.to.bin.DecToBinToHexFragment;
import dec.to.bin.DecimalToBinToHex;
import dec.to.bin.MainActivity;

public class ConversionWorkTest extends
		ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mainActivity;
	private DecimalToBinToHex convert;

	DecToBinToHexFragment fragment;

	TestData testData = new TestData();

	public ConversionWorkTest() {
		super(MainActivity.class);
	}

	public ConversionWorkTest(Class<MainActivity> activityClass) {
		super(MainActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mainActivity = getActivity();
		fragment = (DecToBinToHexFragment) getActivity()
				.getSupportFragmentManager().findFragmentByTag(
						"dectobintohexTag");
		convert = fragment.convert;
	}

	public void testActivityFragmentViewObjectsAreNotNull() {
		assertNotNull(mainActivity);
		assertNotNull(convert);
		assertNotNull(convert.hexXEditText);
		assertNotNull(convert.decimalXEditText);
		assertNotNull(convert.convertButton);
	}

	@MediumTest
	public void testDecimalToBinaryClassWork() {
		for (int i = 0; i < testData.decString.length; i++) {
			BigInteger tempBigInt = new BigInteger(testData.decString[i]);
			String tempString = convert.parseDecimalToBinary(tempBigInt);
			assertTrue(tempString.equals(testData.binString[i]));
		}
	}

	@MediumTest
	public void testDecimalToHexClassWork() {
		for (int i = 0; i < testData.decString.length; i++) {
			BigInteger tempBigInt = new BigInteger(testData.decString[i]);
			String tempString = convert.parseDecimalToHex(tempBigInt);
			assertTrue(tempString.equals(testData.hexString[i]));
		}
	}

	@MediumTest
	public void testBinaryToDecimalClassWork() {
		for (int i = 0; i < testData.decString.length; i++) {
			String binaryString = testData.binString[i];
			String actual = convert.parseBinaryToDecimal(binaryString);
			String expected = testData.decString[i];
			assertTrue(actual.equals(expected));
		}
	}

	@MediumTest
	public void testHexToBinClassWork() {
		for (int i = 0; i < testData.decString.length; i++) {
			String hexString = testData.hexString[i];
			String actual = convert.parseHexToBinary(hexString);
			String expected = testData.binString[i];
			assertTrue(actual.equals(expected));
		}
	}
}